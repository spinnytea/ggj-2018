Universal Inputs
================

Global Game Jam 2018

Our "game" is a collaborative controller, which will allow many people to play one game together.

Think Twitch Plays Pokémon. Or Steam controller remapping.

This works best for simple games but can be used for any game that supports keyboard or xinput
control (dpad + ABXY), and is specified by a loadable configuration (see [Actuators](#Actuators)).

## Dependencies

* [Node.js](nodejs.org)

## Install

```bash
git clone https://bitbucket.org/spinnytea/ggj-2018.git
npm install
```

## Running

```bash
npm run brain -- -r <room_name> <config_file>
```

Sample configuration files are stored in the config folder of the project

# Components

This project consists of three major components: Brain, Relay and Client

## Brain

The brain binds the "controller" to the game and **MUST** run on the same machine that is hosting
the game. A single brain is used to control a single game. Multiple brains can be used at the same time for different games by using different "rooms", which is specified by passing the `-r` flag
when starting the brain.

By default, the brain attempts to use a publicly hosted relay server at [universal-input.herokuapp.com](http://universal-input.herokuapp.com). You can also run a local server (see [Relay](#Relay)), but the brain will need to be configured to connect to it (using the `-s` and `-p` flags).

Brain has a number of other options. To see this in detail, run it as follows:

```bash
npm run brain -- --help
```

### Actuator

The brain also employs an actuator to actually perform the input to the game / system. This is configured as part of the configuration file passed to the brain. Actuators are provided by
this project for keyboard and joystick control; see the sample configurations to see how they're
used.

## Relay

The relay facilitates communication between the Client and Brain, handling interactions in
real time. A single relay can handle any number of games provided they are separated into "rooms". A public relay is hosted at [universal-input.herokuapp.com](http://universal-input.herokuapp.com) and
is the default for the brain.

A local relay can be started at [localhost:3000](http://localhost:3000) by running:

```bash
npm start
```

## Client

The client is the main interactive element used by the players to actually manipulate the
controller. Currently, only one client is implemented, and it is hosted along with the
relay at [universal-input.herokuapp.com](http://universal-input.herokuapp.com). This is also
the case for locally launched relays.

Open the client and enter the name of a room to connect to. The room name **MUST** match the
name of the room used by the brain you wish to connect to. You also **MUST** use the client
hosted by the relay used by the brain; the client assumes that the relay is at the same
location as itself.


# Sample games

Some games we suggest playing (sample configurations are provided):

## Keyboard

* [Asteroids](http://www.dougmcinnes.com/html-5-asteroids)
* [Boxhead](https://www.kongregate.com/games/SeanCooper/boxhead-2play-rooms)
* [Defender](https://archive.org/details/arcade_defender)
* [Dolphin Olympics 2](https://www.kongregate.com/games/arawkins/dolphin-olympics-2)
* [Fast Click Game](https://scratch.mit.edu/projects/820640/)
* [Indiana Jones and the Temple of Doom](https://archive.org/details/arcade_indytemp)

## Gamepad

* [1985](https://wolod.itch.io/1985)
* [Rumble Road](https://seansleblanc.itch.io/rumble-road)

## Others

* [Arcade Games on Archive.org](https://archive.org/details/internetarcade)
