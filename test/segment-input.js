'use strict';
const expect = require('chai').expect;
const segmentInput = require('../lib/brain/segment-input');

describe('segment-input', function () {
  it('def', function () {
    expect(segmentInput).to.be.a('function');
  });

  it('simple test', function () {
    const inputs = ['a', 'b'];
    const client = { id: 'some_hash' };
    const config = { shuffle: true };

    const result = segmentInput(inputs, [client], config);

    expect(result.size).to.equal(1);
    expect(result).to.have.all.keys([client]);
    expect(result.get(client).length).to.equal(2);
    expect(result.get(client)).to.include.members(['a', 'b']);
  });

  describe('four inputs', function () {
    const inputs = ['a', 'b', 'x', 'y'];
    const config = {};

    it('one client', function () {
      const clients = [
        { id: 'btn_1' },
      ];

      const result = segmentInput(inputs, clients, config);

      expect(result.size).to.equal(1);
      expect(result).to.have.all.keys(clients);
      expect(result.get(clients[0]).length).to.equal(4);
      expect(result.get(clients[0])).to.include.members(['a', 'b', 'x', 'y']);
    });

    it('two clients', function () {
      const clients = [
        { id: 'btn_1' },
        { id: 'btn_2' },
      ];

      const result = segmentInput(inputs, clients, config);

      expect(result.size).to.equal(2);
      expect(result).to.have.all.keys(clients);
      expect(result.get(clients[0]).length).to.equal(2);
      expect(result.get(clients[0])).to.include.members(['a', 'x']);
      expect(result.get(clients[1]).length).to.equal(2);
      expect(result.get(clients[1])).to.include.members(['b', 'y']);
    });

    it('three clients', function () {
      const clients = [
        { id: 'btn_1' },
        { id: 'btn_2' },
        { id: 'btn_3' },
      ];

      const result = segmentInput(inputs, clients, config);

      expect(result.size).to.equal(3);
      expect(result).to.have.all.keys(clients);
      expect(result.get(clients[0]).length).to.equal(2);
      expect(result.get(clients[0])).to.include.members(['a', 'y']);
      expect(result.get(clients[1]).length).to.equal(1);
      expect(result.get(clients[1])).to.include.members(['b']);
      expect(result.get(clients[2]).length).to.equal(1);
      expect(result.get(clients[2])).to.include.members(['x']);
    });

    it('four clients', function () {
      const clients = [
        { id: 'btn_1' },
        { id: 'btn_2' },
        { id: 'btn_3' },
        { id: 'btn_4' },
      ];

      const result = segmentInput(inputs, clients, config);

      expect(result.size).to.equal(4);
      expect(result).to.have.all.keys(clients);
      expect(result.get(clients[0]).length).to.equal(1);
      expect(result.get(clients[0])).to.include.members(['a']);
      expect(result.get(clients[1]).length).to.equal(1);
      expect(result.get(clients[1])).to.include.members(['b']);
      expect(result.get(clients[2]).length).to.equal(1);
      expect(result.get(clients[2])).to.include.members(['x']);
      expect(result.get(clients[3]).length).to.equal(1);
      expect(result.get(clients[3])).to.include.members(['y']);
    });
  }); // end four inputs

  describe('overfill', function () {
    it('inputsPerClient NaN', function () {
      const inputs = ['a', 'b'];
      const clients = ['1', '2'];
      const config = {};

      const result = segmentInput(inputs, clients, config);

      expect(result.get('1').length).to.equal(1);
      expect(result.get('1')).to.include.members(['a']);
      expect(result.get('2').length).to.equal(1);
      expect(result.get('2')).to.include.members(['b']);
    });

    it('all have at least one', function () {
      const inputs = ['a', 'b'];
      const clients = ['1', '2'];
      const config = { inputsPerClient: 0.5 };

      const result = segmentInput(inputs, clients, config);

      expect(result.get('1').length).to.equal(1);
      expect(result.get('1')).to.include.members(['a']);
      expect(result.get('2').length).to.equal(1);
      expect(result.get('2')).to.include.members(['b']);
    });

    describe('all inputs used at least once', function () {
      it('one client', function () {
        const inputs = ['a', 'b', 'c', 'd', 'e'];
        const clients = ['1'];
        const config = { inputsPerClient: 1 };

        const result = segmentInput(inputs, clients, config);

        expect(result.get('1').length).to.equal(5);
        expect(result.get('1')).to.include.members(['a', 'b', 'c', 'd', 'e']);
      });

      it('two clients', function () {
        const inputs = ['a', 'b', 'c', 'd', 'e'];
        const clients = ['1', '2'];
        const config = { inputsPerClient: 1 };

        const result = segmentInput(inputs, clients, config);

        expect(result.get('1').length).to.equal(3);
        expect(result.get('1')).to.include.members(['a', 'c', 'e']);
        expect(result.get('2').length).to.equal(2);
        expect(result.get('2')).to.include.members(['b', 'd']);
      });
    }); // end all inputs used at least once

    it('no dup inputs per client', function () {
      const inputs = ['a', 'b', 'c'];
      const clients = ['1', '2', '3'];
      const config = { inputsPerClient: 2 };

      const result = segmentInput(inputs, clients, config);

      expect(result.get('1').length).to.equal(2);
      expect(result.get('1')).to.include.members(['a', 'b']);
      expect(result.get('2').length).to.equal(2);
      expect(result.get('2')).to.include.members(['b', 'c']);
      expect(result.get('3').length).to.equal(2);
      expect(result.get('3')).to.include.members(['c', 'a']);
    });

    describe('stop when clients are full', function () {
      it('one max', function () {
        const inputs = ['a', 'b', 'c'];
        const clients = ['1'];
        const config = { inputsPerClient: 4 };

        const result = segmentInput(inputs, clients, config);

        expect(result.get('1').length).to.equal(3);
        expect(result.get('1')).to.include.members(['a', 'b', 'c']);
      });

      it('multiple max', function () {
        const inputs = ['a', 'b', 'c'];
        const clients = ['1', '2', '3', '4'];
        const config = { inputsPerClient: 4 };

        const result = segmentInput(inputs, clients, config);

        expect(result.get('1').length).to.equal(3);
        expect(result.get('1')).to.include.members(['a', 'b', 'c']);
        expect(result.get('2').length).to.equal(3);
        expect(result.get('2')).to.include.members(['a', 'b', 'c']);
        expect(result.get('3').length).to.equal(3);
        expect(result.get('3')).to.include.members(['a', 'b', 'c']);
        expect(result.get('4').length).to.equal(3);
        expect(result.get('4')).to.include.members(['a', 'b', 'c']);
      });
    }); // end stop when clients are full

    describe('some fill rates', function () {
      const inputs = ['a', 'b', 'c'];
      const clients = ['1', '2', '3', '4'];

      it('small (less)', function () {
        const config = { inputsPerClient: 1.4 };

        const result = segmentInput(inputs, clients, config);

        expect(result.get('1').length).to.equal(2);
        expect(result.get('1')).to.include.members(['a', 'b']);
        expect(result.get('2').length).to.equal(2);
        expect(result.get('2')).to.include.members(['b', 'c']);
        expect(result.get('3').length).to.equal(1);
        expect(result.get('3')).to.include.members(['c']);
        expect(result.get('4').length).to.equal(1);
        expect(result.get('4')).to.include.members(['a']);
      });

      it('small (more)', function () {
        const config = { inputsPerClient: 1.6 };

        const result = segmentInput(inputs, clients, config);

        expect(result.get('1').length).to.equal(2);
        expect(result.get('1')).to.include.members(['a', 'b']);
        expect(result.get('2').length).to.equal(2);
        expect(result.get('2')).to.include.members(['b', 'c']);
        expect(result.get('3').length).to.equal(2);
        expect(result.get('3')).to.include.members(['c', 'a']);
        expect(result.get('4').length).to.equal(1);
        expect(result.get('4')).to.include.members(['a']);
      });

      it('large (less)', function () {
        const config = { inputsPerClient: 2.4 };

        const result = segmentInput(inputs, clients, config);

        expect(result.get('1').length).to.equal(3);
        expect(result.get('1')).to.include.members(['a', 'b', 'c']);
        expect(result.get('2').length).to.equal(3);
        expect(result.get('2')).to.include.members(['a', 'b', 'c']);
        expect(result.get('3').length).to.equal(2);
        expect(result.get('3')).to.include.members(['a', 'c']);
        expect(result.get('4').length).to.equal(2);
        expect(result.get('4')).to.include.members(['a', 'b']);
      });

      it('large (more)', function () {
        const config = { inputsPerClient: 2.6 };

        const result = segmentInput(inputs, clients, config);

        expect(result.get('1').length).to.equal(3);
        expect(result.get('1')).to.include.members(['a', 'b', 'c']);
        expect(result.get('2').length).to.equal(3);
        expect(result.get('2')).to.include.members(['a', 'b', 'c']);
        expect(result.get('3').length).to.equal(3);
        expect(result.get('3')).to.include.members(['a', 'b', 'c']);
        expect(result.get('4').length).to.equal(2);
        expect(result.get('4')).to.include.members(['a', 'b']);
      });
    }); // end some fill rates

    it('allowDuplicateInputs', function () {
      const inputs = ['a'];
      const clients = ['1', '2'];
      const config = { inputsPerClient: 2.5, allowDuplicateInputs: true };

      const result = segmentInput(inputs, clients, config);

      expect(result.get('1').length).to.equal(3);
      expect(result.get('1')).to.deep.equal(['a', 'a', 'a']);
      expect(result.get('2').length).to.equal(2);
      expect(result.get('2')).to.deep.equal(['a', 'a']);
    });
  }); // end overfill
}); // end segment-input
