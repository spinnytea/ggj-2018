'use strict';
const _ = require('lodash');
const debug = require('debug')('brain');
const debugIO = require('debug')('socket.io:brain');
const fs = require('fs');
const io = require('socket.io-client');
const opn = require('opn');
const path = require('path');
const uuid = require('uuid/v4');
const yargs = require('yargs');

const segmentInput = require('./segment-input');

const DEFAULTS = {
  room: uuid(),
  host: 'universal-input.herokuapp.com',
  port: 80,
};

const argv = yargs
  .usage('$0 <options> config')
  .describe('room', 'Name of the room to create')
  .alias('room', 'r')
  .default('room', DEFAULTS.room)
  .describe('host', 'Relay server address')
  .alias('host', 's')
  .default('host', DEFAULTS.host)
  .describe('port', 'Relay server port')
  .alias('port', 'p')
  .number('port')
  .default('port', DEFAULTS.port)
  .demand(1)
  .help()
  .version()
  .argv;

let config = fs.readFileSync(argv._[0]);
config = JSON.parse(config.toString());
config = _.merge({}, argv, config, _.omitBy(argv, (v, k) => DEFAULTS[k] === v));

if (config.url) opn(config.url);

const actuator = require('./fallback-loader')(config.actuator, path.join(__dirname, '..', 'actuators'));
if (!actuator) throw new Error('No actuator loaded');
actuator.config(config);

debug(`Connecting to relay server ${config.host}:${config.port}`);
let socket = io(`http://${config.host}:${config.port}`);
let clients = {};

socket.on('connect' , function () {
  // XXX: Brain is not a typical client (I don't want to know about other brains)
  // announceClient(socket);
  clients = {};
  debugIO('Request other clients');
  socket.emit('$to', config.room, 'announce');
});

// All clients should do this
// socket.on('announce', () => announceClient(socket));

socket.on('$connect', function (clientId) {
  debugIO(`Client connected "${clientId}"`);
  clients[clientId] = true;
  reconfigInput();
});

socket.on('$disconnect', function (clientId) {
  debugIO(`Client disconnected "${clientId}"`);
  delete clients[clientId];
  reconfigInput();
});

function reconfigInput() {
  actuator.reset();
  let clientIds = Object.keys(clients);
  if (clientIds.length) {
    const map = segmentInput(config.inputs, clientIds, config);
    clientIds.forEach((client) => {
      socket.emit('$to', client, 'config', _.merge(
        {},
        _.pick(config, ['showInputText']),
        {
          target: socket.id,
          buttons: map.get(client),
        }
      ));
    });
  }
  if (config.shuffleTimeout && config.shuffleTimeout > 0) {
    clearTimeout(reconfigInput.timeoutId);
    reconfigInput.timeoutId = setTimeout(reconfigInput, config.shuffleTimeout);
  }
}

_.each(config.inputs, (input) => {
  debugIO(`Listening for input "${input}"`);
  socket.on(input, function (state) {
    debug(`Fire "${input}" => "${state}"`);
    if (_.isBoolean(state)) {
      if (state) actuator.down(input);
      else actuator.up(input);
    }
    else actuator.press(input);
  });
});

// function announceClient(sock) {
//   sock.emit('$to', config.room, '$connect', socket.id);
// }