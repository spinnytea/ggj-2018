'use strict';
const _ = require('lodash');
const fs = require('fs');
const path = require('path');

module.exports = function fallbackLoader(filepath, defaultLocalDir) {
  if (_.startsWith(filepath, '~')) {
    try {
      let defaultLocalPath = path.join(defaultLocalDir, `${filepath.substr(1)}.js`);
      fs.statSync(defaultLocalPath);
      return require(defaultLocalPath);
    }
    catch (e) {
      if (!e.code || e.code !== 'MODULE_NOT_FOUND') throw e;
    }
  }
  else if (_.startsWith(filepath, '.') || _.startsWith(filepath, '/')) {
    try { return require(path.resolve(`${filepath}.js`)); }
    catch (e) { void(e); }
  }
  else {
    return require(filepath);
  }
};
