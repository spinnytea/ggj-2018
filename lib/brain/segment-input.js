/**
 * divi up the actuator inputs across the client clients
 */
'use strict';
const _ = require('lodash');

module.exports = segment;

/**
 * @param {String[]} inputs: (tested with strings, but any object should do)
 * @param {Object[]} clients: (probably the clientId, but any object will do)
 * @return Map<client, input[]>;
 */
function segment(inputs, clients, config) {
  const result = new Map();
  clients.forEach((client) => { result.set(client, []); });
  // in the final stages, we remove clients from the list when they are full, don't mutate the sent object
  clients = clients.slice();
  // get a fresh list
  const moreInputs = ()=>(config.shuffle?_.shuffle(inputs):inputs.slice().reverse());
  const moreClients = ()=>(config.shuffle?_.shuffle(clients):clients.slice().reverse());
  let currInputs = moreInputs();
  let currClients = moreClients();
  // handle passing out inputs to clients until a certain saturation
  const inputsPerClient = Math.max(1, (+config.inputsPerClient || 1));
  let overfill = Math.ceil(inputsPerClient * clients.length);

  // first: ensure all clients have an input and all inputs are used
  for(let allInputs = false, allClients = false; !allInputs || !allClients; overfill--) {
    result.get(currClients.pop()).push(currInputs.pop());
    if(_.isEmpty(currInputs)) { allInputs = true; currInputs = moreInputs(); }
    if(_.isEmpty(currClients)) { allClients = true; currClients = moreClients(); }
  }

  // keep streaming clients and inputs until overfill is finished
  const nextInput = () => { if(_.isEmpty(currInputs)) { currInputs = moreInputs(); } return currInputs.pop(); };
  const nextClient = () => { if(_.isEmpty(currClients)) { currClients = moreClients(); } return currClients.pop(); };
  overfill_loop: for(; overfill > 0 && clients.length; overfill--) {
    let client = nextClient();
    if(!config.allowDuplicateInputs) {
      while(result.get(client).length === inputs.length) {
        _.pull(clients, client);
        client = nextClient();
        if(!client) break overfill_loop;
      }
    }
    const clientResult = result.get(client);

    let input = nextInput();
    if(!config.allowDuplicateInputs) while(_.includes(clientResult, input)) input = nextInput();
    clientResult.push(input);
  }

  return result;
}
