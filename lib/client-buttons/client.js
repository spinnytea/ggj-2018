/* global $, _, io */
'use strict';
$(function () {
  // TODO hide the address bar on mobile
  // - OORR, change the layout, we can't see the bottom button
  const socket = io();
  const name = getRoomName();
  // const name = 'hoopyfrood'; void(getRoomName);
  let config = { buttons: [] };

  // Backgrounds for prettiness
  const buttonBackgrounds = [ 'pink', 'teal', 'lime', 'navy', 'orange', 'mediumaquamarine', 'firebrick', 'mediumpurple' ];
  $('#emptyRoomMessage').text(_.sample([
    "You're the first one here.",
    'You have created a universe.',
    "You're all by yourself.",
    'LEEEEEEEEROY JENKINS',
  ]));

  socket.on('connect', function () {
    announceClient(socket, name);
  });

  function getRoomName() {
    var promptedName;
    var keepChecking = true;
    while(keepChecking) {
      promptedName = prompt('Enter 8-character Room Name:');
        if (_.isString(promptedName)) {
        promptedName = promptedName.trim();
          if (promptedName.length >= 8) {
              keepChecking = false;
          }
      }
    }
    return promptedName;
  }

  socket.on('announce', () => { announceClient(socket, name); });

  function announceClient(sock, room) {
    sock.emit('$to', room, '$connect', socket.id);
  }

  socket.on('config', function (conf) {
    console.log('Update config', conf);
    config = conf;
    formatAndShowButtons();
    $('#emptyRoomMessage').css('display', 'none');
  });

  function formatAndShowButtons(){
    $('[role="button"]').attr('class', 'bc'+(Math.min(config.buttons.length,8)));
    const backgrounds = _.shuffle(buttonBackgrounds);
    backgrounds.forEach(function (color, index) {
      $('#b' + (index+1) + '>.shaker')
        .css('background', color)
        .text(config.showInputText ? config.buttons[index] : '');
    });
  }

  // Emit either a true for mousedown or false for mouseup
  function doEmit(index, down) {
    return function () {
      // TODO enable button shake when down
      if(config.buttons.length < index) return false;
      socket.emit('$to', config.target, config.buttons[index-1], down);
      if (down) {
        $('#b'+index + '>.shaker').addClass('shake');
      }
      else {
        $('#b'+index + '>.shaker').removeClass('shake');
      }
    };
  }

  // Set up button press states
  // base of 1, not 0
  for (let i=1; i<9; i++) {
    $('#b'+i)
      .mousedown(doEmit(i, true))
      .mouseup(doEmit(i, false))
      .bind('touchstart', doEmit(i, true))
      .bind('touchend', doEmit(i, false))
      .bind('mouseleave', doEmit(i, false))
    ;
  }

});
