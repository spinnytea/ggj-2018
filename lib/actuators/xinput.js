'use strict';
const _ = require('lodash');
const debug = require('debug')('actuator:xinput');
const SerialPort = require('serialport');
const Actuator = require('./actuator');

const KEYMASKS = {
  up: 0x01,
  down: 0x02,
  left: 0x04,
  right: 0x08,
  a: 0x10,
  b: 0x20,
  x: 0x40,
  y: 0x80,
};

class XInputActuator extends Actuator {
  constructor() {
    super();
    this.buff = Buffer.allocUnsafe(1);
    this.port = null;
    this.reset();
  }

  config(config) {
    this.config = config;
    this.port = new SerialPort(config.device, { baudRate: config.baudRate });
  }

  down(key) {
    this.inputs[key]++;
    this.updateMask();
  }

  up(key) {
    this.inputs[key]--;
    this.updateMask();
  }

  reset() {
    this.inputs = {
      up: 0,
      down: 0,
      left: 0,
      right: 0,
      a: 0,
      b: 0,
      x: 0,
      y: 0,
    };
    this.updateMask();
  }

  updateMask() {
    let mask = 0;
    if (this.config.vote) mask = KEYMASKS[_.maxBy(_.toPairs(this.inputs), (pair) => pair[1])[0]];
    else {
      _.each(this.inputs, (count, input) => {
        if (count > 0) mask = mask | KEYMASKS[input];
      });
    }
    this.buff.writeUInt8(mask);
    debug(`Send mask "0x${mask.toString(16)}"`);
    if (this.port) {
      this.port.write(this.buff);
    }
  }
}

module.exports = new XInputActuator();