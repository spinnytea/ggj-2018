'use strict';
const debug = require('debug')('actuator');

class Actuator {
  config(config) {
    debug('Actuator config no-op', config);
  }
  
  press(key) {
    debug(`Actuator press "${key}"`);
  }
  
  down(key) {
    debug(`Actuator down "${key}"`);
  }
  
  up(key) {
    debug(`Actuator up "${key}"`);
  }
  
  reset() {
    debug('Actuator reset');
  }
}

module.exports = Actuator;