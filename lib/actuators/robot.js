/** implemenation of an actuator that uses python for key events */
'use strict';
const _ = require('lodash');
const robot = require('robotjs');
const Actuator = require('./actuator');

const MOUSE_BUTTONS = { 'leftclick': 'left', 'rightclick': 'right', 'middleclick': 'middle' };

class RobotActuator extends Actuator {
  constructor() {
    super();
    this.CURRENTLY_PRESSED = {};
  }

  press(key) {
    if(key in MOUSE_BUTTONS) {
      robot.mouseClick(MOUSE_BUTTONS[key]);
    }
    else {
      robot.keyTap(key);
    }
  }

  down(key) {
    if(key in MOUSE_BUTTONS) {
      robot.mouseToggle('down', MOUSE_BUTTONS[key]);
    }
    else {
      robot.keyToggle(key, 'down');
    }
    this.CURRENTLY_PRESSED[key] = true;
  }

  up(key) {
    if(key in MOUSE_BUTTONS) {
      robot.mouseToggle('up', MOUSE_BUTTONS[key]);
    }
    else {
      robot.keyToggle(key, 'up');
    }
    delete this.CURRENTLY_PRESSED[key];
  }

  reset() {
    Object.keys(this.CURRENTLY_PRESSED).forEach((key) => this.up(key));
  }
}

module.exports = new RobotActuator();
