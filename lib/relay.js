'use strict';
const _ = require('lodash');
const debug = require('debug')('socket.io:relay');

module.exports = configureRelay;

function configureRelay(io) {
  io.on('connection', function (socket) {
    const rooms = {};
    rooms[socket.id] = true;

    socket.on('$join', joinRoom);
    socket.on('$leave', leaveRoom);

    function joinRoom(room, cb) {
      if (!rooms[room]) {
        rooms[room] = true;
        debug(`Socket "${socket.id}" join room "${room}"`);
        socket.join(room, () => { if (cb) cb(); });
      }
      else cb();
    }

    function leaveRoom(room, cb) {
      if (rooms[room]) {
        delete rooms[room];
        debug(`Socket "${socket.id}" leave room "${room}"`);
        socket.leave(room, () => { if (cb) cb(); });
      }
      else cb();
    }

    socket.on('$broadcast', function (topic, ...args) {
      let fn = (_.isFunction(args.slice(-1))) ? args.pop() : null;
      debug(`Broadcast on "${topic}"`, ...args);
      socket.broadcast.emit(topic, ...args);
      if (fn) fn();
    });

    socket.on('$to', function (target, topic, ...args) {
      joinRoom(target, function () {
        let fn = (_.isFunction(args.slice(-1))) ? args.pop() : null;
        debug(`Broadcast to room "${target}" on "${topic}"`, ...args);
        socket.broadcast.to(target).emit(topic, ...args);
        if (fn) fn();
      });
    });

    socket.on('disconnect', function () {
      debug(`Disconnect "${socket.id}"`);
      _.each(rooms, (__, room) => { socket.broadcast.to(room).emit('$disconnect', socket.id); });
    });

    socket.emit('$ready');
  });
}