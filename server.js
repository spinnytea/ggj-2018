/* eslint no-console: ["off"] */
'use strict';
const path = require('path');
const express = require('express');
const expressLess = require('express-less');

const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

const port = process.env['PORT'] || 3000;

// Dynamically generate style
app.use('/style/client-buttons', expressLess(
  path.join(__dirname, 'lib', 'client-buttons'),
  { compress: true }
));

// XXX use a CDN instead of node_modules
app.use('/jquery.min.js', express.static(path.join(__dirname, 'node_modules', 'jquery', 'dist', 'jquery.min.js')));
app.use('/lodash.min.js', express.static(path.join(__dirname, 'node_modules', 'lodash', 'lodash.min.js')));
app.use('/', express.static(path.join(__dirname, 'lib', 'client-buttons')));

require('./lib/relay')(io);

http.listen(port, () => {
  console.log(`Running on port ${port}`);
});
